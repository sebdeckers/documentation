module.exports = () => ({
  https: {
    port: 44433
  },
  http: {
    redirect: false
  },
  hosts: [
    {
      directories: {
        trailingSlash: 'always'
      },
      manifest: 'serverpush.json'
    }
  ]
})
