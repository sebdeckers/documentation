const { spawn } = require('child_process')
const { promisify } = require('util')
const { join, dirname } = require('path')
const { createWriteStream, readFile } = require('fs')
const mkdirp = require('mkdirp')
const { extract } = require('tar-stream')
const concat = require('concat-stream')
const h = require('hastscript')
const unified = require('unified')
const remarkParse = require('remark-parse')
const remarkHighlight = require('remark-highlight.js')
const remarkToc = require('remark-toc')
const remarkRehype = require('remark-rehype')
const remarkSlug = require('remark-slug')
const rehypeFormat = require('rehype-format')
const rehypeAutolinkHeadings = require('rehype-autolink-headings')
const rehypeStringify = require('rehype-stringify')
const hastUtilSelect = require('hast-util-select')
const hastUtilHeading = require('hast-util-heading')
const hastUtilToString = require('hast-util-to-string')
const unistUtilMap = require('unist-util-map')
const unistUtilFind = require('unist-util-find')
const unistRemove = require('unist-util-remove')

const repositories = [
  { remote: 'git@gitlab.com:commonshost/cli.git' },
  { remote: 'git@gitlab.com:commonshost/server.git' },
  { remote: 'git@gitlab.com:commonshost/manifest.git' },
  { remote: 'git@gitlab.com:commonshost/core.git' }
  // { remote: 'file:///Users/sebdeckers/code/commonshost/cli' },
  // { remote: 'file:///Users/sebdeckers/code/commonshost/server' },
  // { remote: 'file:///Users/sebdeckers/code/commonshost/manifest' },
  // { remote: 'file:///Users/sebdeckers/code/commonshost/core' }
]

function fileRaw ({
  repository: {
    remote,
    branch = 'HEAD',
    path = [ 'README.md', 'docs/*.md' ]
  }
}) {
  return new Promise(async (resolve, reject) => {
    const command = ['archive', '--remote', remote, branch, ...path]
    const git = spawn('git', command)
    git.on('error', reject)
    const untar = extract()
    git.stderr.pipe(process.stderr)
    git.stdout.pipe(untar)
    const files = []
    untar.on('entry', async (header, stream, next) => {
      if (header.type !== 'file') return next()
      stream.on('error', reject)
      stream.pipe(concat((buffer) => {
        files.push({
          source: header.name,
          raw: buffer
        })
        next()
      }))
    })
    untar.on('finish', () => {
      return files.length > 0
        ? resolve(files)
        : reject(new Error('Failed to load files.'))
    })
  })
}

function fileDestination (file) {
  const prefix = 'docs/'
  let destination = file.source
    .replace(/README\.md$/i, 'index.html')
    .replace(/\.md$/i, '.html')
  if (destination.startsWith(prefix)) {
    destination = destination.substr(prefix.length)
  }
  return { ...file, destination }
}

function filePath (file) {
  const path = file.destination.replace(/index.html$/, '')
  return { ...file, path }
}

async function processMarkdown (file) {
  if (!file.source.match(/\.md$/i)) {
    return file
  }
  let toc, title
  const processor = unified()
    .use(remarkParse)
    .use(remarkHighlight)
    .use(remarkSlug)
    .use(remarkToc, { maxDepth: 2 })
    .use(remarkRehype)
    .use(rehypeAutolinkHeadings)
    .use((options) => (node) => {
      const heading = hastUtilSelect.select('#table-of-contents', node)
      const list = hastUtilSelect.select('#table-of-contents ~ ul', node)
      if (file.destination === 'index.html') {
        unistRemove(node, (child) => child === heading)
        unistRemove(node, (child) => child === list)
      }
      toc = list
    })
    .use((options) => (node) => {
      const heading = unistUtilFind(node, hastUtilHeading)
      title = hastUtilToString(heading)
    })
  const markdown = file.raw.toString()
    .replace('\n## ', '\n## Table of Contents\n\n## ')
  const tree = await processor.parse(markdown)
  const hast = await processor.run(tree)
  return { toc, title, hast, ...file }
}

function getNavigation (project) {
  return h('ul', project.files
    .filter(({ source }) => source.endsWith('.md'))
    .filter(({ destination }) => destination !== 'index.html')
    .map((file) => file.toc && h('li', [
      h('a', { href: `/${project.path}/${file.path}` }, file.title),
      unistUtilMap(file.toc, (node, index, parent) => {
        if (node.type === 'element' && node.tagName === 'a') {
          return {
            ...node,
            properties: {
              ...node.properties,
              href: `/${project.path}/${file.path}${node.properties.href}`
            }
          }
        }
        return node
      })
    ]))
    .filter(Boolean)
  )
}

async function getProjects (repositories) {
  let projects = repositories
    .map((repository) => ({ repository }))
  projects = projects.map((project) => {
    const path = project.repository.remote
      .match(/\/([\w\d]+)(\.git)?$/)[1]
    return { ...project, path }
  })
  projects = await Promise.all(projects.map(async (project) => {
    let files = await fileRaw(project)
    files = files.map(fileDestination)
    files = files.map(filePath)
    files = await Promise.all(files.map(processMarkdown))
    return { ...project, files }
  }))
  projects = projects.map((project) => {
    const index = project.files
      .find(({ destination }) => destination === 'index.html')
    const toc = getNavigation(project)
    index.hast.children.push(
      h('h2', { id: 'table-of-contents' }, [
        h('a', { 'aria-hidden': 'true', href: '#table-of-contents' }, [
          h('span', { class: 'icon icon-link' })
        ]),
        'Table of Contents'
      ]),
      toc
    )
    const { title } = index
    return { ...project, title }
  })
  return projects
}

async function hastToHtml (node) {
  const processor = await unified()
    .use(rehypeFormat)
    .use(rehypeStringify)
  const formatted = await processor.run(node)
  const html = await processor.stringify(formatted)
  return html
}

async function getOverview () {
  const raw = await promisify(readFile)(join(__dirname, 'README.md'), 'utf8')
  const file = await processMarkdown({
    source: 'README.md',
    destination: 'index.html',
    title: 'Overview',
    raw: raw.replace(/^# .+$/m, '# Overview\n')
  })
  return file
}

async function main () {
  const projects = await getProjects(repositories)
  const navigation = await hastToHtml(
    h('ul', projects.map((project) => h('li', [
      h('a', { href: `/${project.path}/` }, project.title.split(' ')[0]),
      getNavigation(project)
    ])))
  )
  const templates = {
    page: await promisify(readFile)(join(__dirname, 'templates/page.html'), 'utf8')
  }

  const overview = await getOverview()
  overview.hast.children.push(
    h('ul', { id: 'projects-overview' }, projects.map((project) => {
      return h('li', { class: project.path }, [
        h('a', { href: `/${project.path}/` }, project.title)
      ])
    }))
  )
  projects.push({
    url: 'https://commons.host',
    path: '',
    title: 'Commons Host',
    files: [overview]
  })

  for (const project of projects) {
    for (const file of project.files) {
      const base = join(process.cwd(), 'public')
      const filepath = join(base, project.path, file.destination)
      console.log('--', filepath)
      await promisify(mkdirp)(dirname(filepath))
      const output = createWriteStream(filepath)
      if (file.hast) {
        const content = await hastToHtml(file.hast)
        const title = file.title === project.title || !project.title
          ? file.title : `${file.title} - ${project.title}`
        const substitutions = {
          'page.content': content,
          'page.title': title,
          'project.path': project.url || `/${project.path}/`,
          'project.title': project.title,
          'site.navigation': navigation
        }
        const html = templates.page.replace(
          /{{\s*([-.\w]+)\s*}}/g,
          (match, p1) => substitutions[p1]
        )
        output.write(html)
      } else {
        output.write(file.raw)
      }
      output.end()
    }
  }
}

main()
